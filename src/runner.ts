import socks from "socksv5";
import { debug } from "util";

import { Application, getApplication } from "./application";

const app: Application = getApplication();

const server = socks.createServer((info, accept, deny) => {
  accept();
});

server.listen(app.config.port, "localhost", () => {
  debug(`SOCKS server listening on port ${app.config.port}`);
});

server.useAuth(
  socks.auth.UserPassword((user, password, cb) => {
    cb(user === app.auth.name && password === app.auth.password);
  })
);
