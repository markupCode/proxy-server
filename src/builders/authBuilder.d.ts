import Auth from "../models/auth";

export default interface AuthBuilder {
  setName(name: string): AuthBuilder;
  setPassword(password: string): AuthBuilder;
  build(): Auth;
}
