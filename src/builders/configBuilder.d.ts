import Config from "../models/config";

export default interface ConfigBuilder {
  setPort(port: number): ConfigBuilder;
  build(): Config;
}
