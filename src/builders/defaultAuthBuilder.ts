import Auth from "../models/auth";
import AuthBuilder from "./authBuilder";

export default class DefaultAuthBuilder implements AuthBuilder {
  private _auth: Auth;

  constructor() {
    this.reset();
  }

  public reset() {
    this._auth = new Auth();
  }

  public setName(name: string): AuthBuilder {
    this._auth.name = name;
    return this.nextStep();
  }
  public setPassword(password: string): AuthBuilder {
    this._auth.password = password;
    return this.nextStep();
  }
  public build(): Auth {
    const auth = this._auth;
    this.reset();
    return auth;
  }

  private nextStep(): AuthBuilder {
    return this;
  }
}
