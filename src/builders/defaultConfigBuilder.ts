import Config from "../models/config";
import ConfigBuilder from "./configBuilder";

export default class DefaultConfigBuilder implements ConfigBuilder {
  private _config: Config;

  constructor() {
    this.reset();
  }

  public reset() {
    this._config = new Config();
  }

  public setPort(port: number): ConfigBuilder {
    this._config.port = port;
    return this.nextStep();
  }

  public build(): Config {
    const config = this._config;
    this.reset();
    return config;
  }

  private nextStep(): DefaultConfigBuilder {
    return this;
  }
}
