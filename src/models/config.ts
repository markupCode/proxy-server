export default class Config {
  private _port: number = 1080;
  get port() {
    return this._port;
  }
  set port(value) {
    if (!this.validPort(value))
      throw new Error(`invalid port number: ${value}`);
    this._port = value;
  }

  private validPort(value: number) {
    if (value < 0 && value > 64000) return false;
    if (value % 1 !== 0) return false;
    return true;
  }
}
