import { isNullOrUndefined } from "util";

export default class EnvironmentStorage {
  public getVariable(key: string): string {
    const variable = process.env[key];
    if (isNullOrUndefined(variable))
      throw new Error(`environment variable was not initialized: ${key}`);
    return variable;
  }
}
