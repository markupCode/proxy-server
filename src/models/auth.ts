export default class Auth {
  private _name: string;
  get name() {
    return this._name;
  }
  set name(value) {
    this._name = value;
  }

  private _password: string;
  get password() {
    return this._password;
  }
  set password(value) {
    this._password = value;
  }
}
