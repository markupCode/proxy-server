import DefaultAuthBuilder from "./builders/defaultAuthBuilder";
import DefaultConfigBuilder from "./builders/defaultConfigBuilder";

import Auth from "./models/auth";
import Config from "./models/config";
import Storage from "./models/environmentStorage";

export class Application {
  private _auth: Auth;
  get auth() {
    return this._auth;
  }

  private _config: Config;
  get config() {
    return this._config;
  }

  constructor(auth: Auth, config: Config) {
    this._auth = auth;
    this._config = config;
  }
}

enum Variable {
  NAME = "AUTH_NAME",
  PASSWORD = "AUTH_PASSWORD",
  PORT = "PORT"
}

function getStorage(): Storage {
  return new Storage();
}

function getAuth(storage: Storage): Auth {
  return new DefaultAuthBuilder()
    .setName(storage.getVariable(Variable.NAME))
    .setPassword(storage.getVariable(Variable.PASSWORD))
    .build();
}

function getConfig(storage: Storage): Config {
  return new DefaultConfigBuilder()
    .setPort(parseInt(storage.getVariable(Variable.PORT), 10))
    .build();
}

export const getApplication = () => {
  const storage = getStorage();
  return new Application(getAuth(storage), getConfig(storage));
};
